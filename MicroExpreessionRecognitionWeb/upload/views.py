
from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
import os
import cv2
import numpy as np
from .FER.src.utils import index2emotion, cv2_img_add_text
from MicroExpreessionRecognitionWeb import settings

from .FER.src.model import CNN3,model_3

def index(request):
    return render(request,'index.html')
    # return HttpResponse("Hello, world!")

def uploadFacePic(request):
    #return HttpResponse("faceInfo")
    if request.method == "POST":
        f1 = request.FILES['pic']
        fname = '%s/input/pic/%s' % (settings.MEDIA_ROOT, f1.name)
        with open(fname, 'wb') as pic:
            for c in f1.chunks():
                pic.write(c)
        # model=CNN3()
        model=model_3()
        model.load_weights('./upload/FER/models/model_3_weights.h5')
        result = predict_expression(f1.name,fname, model)
        print(result)
        return JsonResponse({
            "msg":"ok",
            "predictResult":result[0],
            "picUrl":"/media/output/"+f1.name,
        })
    else:
        return JsonResponse({
            "msg":"error",
        })




def predict_expression(pic_name,img_path, model):
    """
    对图中n个人脸进行表情预测
    :param img_path:
    :return:
    """
    #print("predict_expression")
    border_color = (0, 0, 0)  # 黑框框
    font_color = (255, 255, 255)  # 白字字

    img, img_gray, faces = face_detect(img_path)
    if len(faces) == 0:
        return 'no', [0, 0, 0, 0, 0, 0, 0, 0]
    # 遍历每一个脸
    emotions = []
    result_possibilitys = []
    for (x, y, w, h) in faces:
        face_img_gray = img_gray[y:y + h + 10, x:x + w + 10]
        faces_img_gray = generate_faces(face_img_gray)
        # 预测结果线性加权
        results = model.predict(faces_img_gray)
        result_sum = np.sum(results, axis=0).reshape(-1)
        label_index = np.argmax(result_sum, axis=0)
        emotion = index2emotion(label_index, 'en')
        cv2.rectangle(img, (x - 10, y - 10), (x + w + 10, y + h + 10), border_color, thickness=2)
        img = cv2_img_add_text(img, emotion, x + 30, y + 30, font_color, 20)
        emotions.append(emotion)
        result_possibilitys.append(result_sum)
    if not os.path.exists("./upload/FER/output"):
        os.makedirs("./upload/FER/output")

    cv2.imwrite('./upload/FER/output/'+pic_name, img)
    return emotions[0], result_possibilitys[0]


def face_detect(img_path):
    """
    检测测试图片的人脸
    :param img_path: 图片的完整路径
    :return:
    """

    face_cascade = cv2.CascadeClassifier('./upload/FER/dataset/params/haarcascade_frontalface_alt.xml')
    img = cv2.imread(img_path)

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(
        img_gray,
        scaleFactor=1.1,
        minNeighbors=1,
        minSize=(30, 30)
    )
    return img, img_gray, faces


def generate_faces(face_img, img_size=48):
    """
    将探测到的人脸进行增广
    :param face_img: 灰度化的单个人脸图
    :param img_size: 目标图片大小
    :return:
    """
    face_img = face_img / 255.
    face_img = cv2.resize(face_img, (img_size, img_size), interpolation=cv2.INTER_LINEAR)
    resized_images = list()
    resized_images.append(face_img[:, :])
    resized_images.append(face_img[2:45, :])
    resized_images.append(cv2.flip(face_img[:, :], 1))
    # resized_images.append(cv2.flip(face_img[2], 1))
    # resized_images.append(cv2.flip(face_img[3], 1))
    # resized_images.append(cv2.flip(face_img[4], 1))
    resized_images.append(face_img[0:45, 0:45])
    resized_images.append(face_img[2:47, 0:45])
    resized_images.append(face_img[2:47, 2:47])

    for i in range(len(resized_images)):
        resized_images[i] = cv2.resize(resized_images[i], (img_size, img_size))
        resized_images[i] = np.expand_dims(resized_images[i], axis=-1)
    resized_images = np.array(resized_images)
    return resized_images


