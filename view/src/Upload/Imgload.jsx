import React,{useState} from 'react'
import { Upload,Button,message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import styled from 'styled-components';


export default function Imgload(prop) {
  const Imgload = styled.div`
    position: fixed;
    bottom: 0;
    left: 0;
  `
  const {commitImg} = prop
  const [type,setType] = useState('')
  
  const commit = () => {
    commitImg(type)
  }
  const props = {
    name: 'pic',
    action: 'http://127.0.0.1:8000/info/',
    method:'post',
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {

        if(info.file.response.predictResult === 'no') {
          message.error(`表情识别失败`);
          return
        }
        message.success(`${info.file.name} 上传成功`);
        setType(info.file.response.predictResult)
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`);
      }
    },
  };
  return (
    <Imgload>
      <Button onClick={commit}>查看结果</Button>
      <Upload {...props}>
        <Button icon={<UploadOutlined />} style={{marginBottom:`${20}px`}}>上传</Button>
      </Upload>
    </Imgload>
  )
}
