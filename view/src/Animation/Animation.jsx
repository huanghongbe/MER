import React from 'react'
import './Animation.css' 
import styled from 'styled-components';
export default function Animation(props) {
  const Animation = styled.div`
    text-align: center;
    margin: 80px auto 0;
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
  `

  const {type} = props

  const neutral = (
    <div className="emoji  emoji--yay" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__face">
        <div className="emoji__eyebrows"></div>
        <div className="emoji__mouth"></div>
      </div>
    </div>
  )
  const disgust = (
    <div className="emoji  emoji--like" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__hand">
      </div>
    </div>
  )
  const fear = (
    <div className="emoji  emoji--love" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__heart"></div>
    </div>
  )
  const happy = (
    <div className="emoji  emoji--haha" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__face">
        <div className="emoji__eyes"></div>
        <div className="emoji__mouth">
          <div className="emoji__tongue"></div>
        </div>
      </div>
    </div>
  )
  const surprised = (
    <div className="emoji  emoji--wow" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__face">
        <div className="emoji__eyebrows"></div>
        <div className="emoji__eyes"></div>
        <div className="emoji__mouth"></div>
      </div>
    </div>
  )
  const sad = (
    <div className="emoji  emoji--sad" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__face">
        <div className="emoji__eyebrows"></div>
        <div className="emoji__eyes"></div>
        <div className="emoji__mouth"></div>
      </div>
    </div>
  )
  const anger = (
    <div className="emoji  emoji--angry" style={{transform:`scale(${3.0})`}}>
      <div className="emoji__face">
        <div className="emoji__eyebrows"></div>
        <div className="emoji__eyes"></div>
        <div className="emoji__mouth"></div>
      </div>
    </div>
  )
  const emoji = {
    neutral,
    disgust,
    fear,
    happy,
    surprised,
    sad,
    anger
  }
  return (
    <Animation>
      {emoji[type]}
    </Animation>
  )
}
