import React,{useState} from 'react'
import styled from 'styled-components'
import Upload from './Upload/Imgload'
import Animation from './Animation/Animation'
// const emoji = {
  
// };
export default function App() {
  const App = styled.div`
    background: #224059;
    display: grid;
    justify-content: center;
    align-content: center;
    width: 100vw;
    height: 100vh;
  `
  const [type,setType] = useState('')
  const commitImg = (data) => {
    setType(data)
  }
  return (
    <App>
      <Upload commitImg={commitImg}></Upload>
      <Animation type={type}/>
    </App>
  )
}
